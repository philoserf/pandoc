# Pandoc Dockerfile

[![Docker Hub Automated build][hubbadge]][hublisting]
[![Quay.io Automated build][quaybadge]][quaylisting]

Example in use:

```shell
docker run --volume $PWD:$PWD --workdir $PWD --interactive --tty --rm \
  pandoc --from=markdown --to=gfm --output=README.md README.md
```

- [Dockerfile](./Dockerfile)
- [Pandoc Site](http://pandoc.org/)
- [Pandoc Code](https://github.com/jgm/pandoc)

[hublisting]: https://hub.docker.com/r/philoserf/pandoc/builds/
[hubbadge]: https://img.shields.io/badge/docker_hub-automated_build-blue.svg
[quaylisting]: https://quay.io/repository/philoserf/pandoc
[quaybadge]: https://img.shields.io/badge/quay-automated_build-blue.svg
